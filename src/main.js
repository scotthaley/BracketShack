import Vue from 'vue'
import App from './App'
import router from './router'

const firebase = require('firebase')
firebase.initializeApp({
  apiKey: 'AIzaSyAPq54cJZ1OSeeNaB_zBm5yOFcs8o2jcQs',
  authDomain: 'bracketshack.firebaseapp.com',
  databaseURL: 'https://bracketshack.firebaseio.com',
  projectId: 'bracketshack',
  storageBucket: 'bracketshack.appspot.com',
  messagingSenderId: '369847286304'
})

Vue.config.productionTip = false

require('@/assets/pacifico.css')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
