import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Pages/Landing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BracketShack.com',
      component: Landing
    }
  ]
})
